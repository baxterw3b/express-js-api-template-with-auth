/**
 * ErrorsMiddleware
 * developed by: Luca Chiesa
 *
 * Params
 * @param {Object/String} err
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 */

const { Unauthorized, BadRequest, NotFound } = require('http-errors')
const { check, validationResult } = require('express-validator')
const { verifyToken } = require('../util')


class Middleware {
  constructor() {

    this.createUserValidator = [
      check('email').normalizeEmail().isEmail().withMessage('invalid email'),
      check('password').isString().isLength({ min: 4, max: 50 }).withMessage('invalid password, has to be a string and has to be min 3 characters and max 50 characters long')
    ]

    this.findUserByIdValidator = [
      check('id').isInt().withMessage('invalid id, should be an integer')
    ]

    this.findUserByEmailValidator = [
      check('email').isEmail().withMessage('invalid email')
    ]

    this.removeUserValidator = [
      check('id').isInt().withMessage('invalid id, should be an integer')
    ]

    this.updateUserValidator = [
      check('email').optional().normalizeEmail().isEmail().withMessage('invalid email'),
      check('password').optional().isString().withMessage('invalid password, has to be a string and has to be min 3 characters and max 50 characters long')
    ]
  }

  errors(err, req, res, next) {
    const status = err.status || 500
    console.log(status, err.message || err)
    if (status !== 500) {
      if (typeof err === 'object') res.status(status).json({ code: status, message: err.message })
      else res.status(status).json({ code: status, message: err })
    } else res.status(status).json({ code: 500, message: `Internal server error` })
  }

  notFound(req, res, next) {
    next(NotFound(`Cannot ${req.method} ${req.path}`))
  }

  async verifyToken(req, res, next) {
    try {
      if (!req.headers.authorization) throw Unauthorized('Please provide a valid token')
      const decoded = await verifyToken(req.headers.authorization)
      req.user = { id: decoded.id, email: decoded.email }
      next()
    } catch (err) {
      throw BadRequest(err.message)
    }
  }

  validate(req, res, next) {
    const result = validationResult(req)
    if (result.errors.length) throw BadRequest(result.errors)
    else next()
  }
}

module.exports = new Middleware()