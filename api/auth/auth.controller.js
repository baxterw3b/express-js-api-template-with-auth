/**
 * AuthController
 * developed by: Luca Chiesa
 */

//Dependencies
const AuthService = require('./auth.service')

//Controller
class AuthController {

  async signup(req, res) {
    const user = await AuthService.signup(req.body)
    res.status(200).json({ code: 200, data: user })
  }

  async signin(req, res) {
    const user = await AuthService.signin(req.body)
    res.status(200).json({ code: 200, data: user })
  }
}

//Exports
module.exports = new AuthController()