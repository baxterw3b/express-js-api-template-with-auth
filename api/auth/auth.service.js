/**
 * AuthService
 * developed by: Luca Chiesa
 */

//Dependencies
const { BadRequest } = require('http-errors')
const UsersService = require('../users/users.service')

//Service
class AuthService {
  async signup(body) {
    let user = UsersService.userExists(body.email)
    if (user) throw BadRequest(`Email already taken`)
    user = await UsersService.create(body)
    return user
  }

  async signin(body) {
    const { email, password } = body
    const user = UsersService.getInstance({ email })
    if (!user) throw BadRequest(`Invalid credentials`)
    const isPasswordValid = await user.comparePassword(password)
    if (!isPasswordValid) throw BadRequest(`Invalid credentials`)
    user.token = await user.generateToken()
    return user.toJson({ token: true })
  }
}

//Exports
module.exports = new AuthService()
