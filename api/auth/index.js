/**
 * AuthRoutes
 * developed by: Luca Chiesa
 */

//Dependencies
const express = require('express')
const router = express.Router()
const AuthController = require('./auth.controller')
const Middleware = require('../../middlewares')

//Routes
router
  .post('/signup', Middleware.createUserValidator, Middleware.validate, AuthController.signup)
  .post('/signin', AuthController.signin)

//Exports
module.exports = router