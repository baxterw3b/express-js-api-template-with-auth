/**
 * UsersController
 * developed by: Luca Chiesa
 */

//Dependencies
const UsersService = require('./users.service')

//Controller
class UsersController {

  findAll(req, res) {
    const users = UsersService.findAll()
    res.json({ code: 200, data: users })
  }

  findById(req, res) {
    const user = UsersService.findById(req.params.id)
    return res.status(200).json({ code: 200, data: user })
  }

  findByEmail(req, res) {
    const user = UsersService.findByEmail(req.body.email)
    return res.status(200).json({ code: 200, data: user })
  }

  async create(req, res) {
    const user = await UsersService.create(req.body)
    return res.status(200).json({ code: 200, data: user })
  }

  update(req, res) {
    const user = UsersService.update(req.params.id, req.body)
    return res.status(200).json({ code: 200, data: user })
  }

  remove(req, res) {
    UsersService.remove(req.params.id)
    return res.status(200).json({ code: 200, data: 'User deleted successfully' })
  }
}

//Exports
module.exports = new UsersController()