/**
 * UserEntity
 * developed by: Luca Chiesa
 */

//Dependencies
const { hashPassword, comparePassword, generateToken } = require('../../util')

let id = 0

//Entity
class User {
  constructor(email, password) {
    this.id = ++id
    this.email = email
    this.password = password
    this.token = null
  }

  async hashPassword() {
    try {
      return await hashPassword(this.password)
    } catch (err) {
      throw err
    }
  }

  async comparePassword(password) {
    try {
      return await comparePassword(password, this.password)
    } catch (err) {
      throw err
    }
  }

  generateToken() {
    return generateToken({ id: this.id, email: this.email })
  }

  toJson(options) {
    let data = {
      id: this.id,
      email: this.email
    }
    if (options && options.token) data.token = this.token
    return data
  }
}

//Exports
module.exports = User