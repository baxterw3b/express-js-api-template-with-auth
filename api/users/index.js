/**
 * UsersRoutes
 * developed by: Luca Chiesa
 */

//Dependencies
const express = require('express')
const router = express.Router()
const UsersController = require('./users.controller')
const Middleware = require('../../middlewares')

//Routes
router
  .use(Middleware.verifyToken)
  .get('/', UsersController.findAll)
  .post('/', Middleware.createUserValidator, Middleware.validate, UsersController.create)
  .get('/:id', Middleware.findUserByIdValidator, Middleware.validate, UsersController.findById)
  .post('/email', Middleware.findUserByEmailValidator, Middleware.validate, UsersController.findByEmail)
  .patch('/:id', Middleware.updateUserValidator, Middleware.validate, UsersController.update)
  .delete('/:id', Middleware.removeUserValidator, Middleware.validate, UsersController.remove)


//Exports
module.exports = router