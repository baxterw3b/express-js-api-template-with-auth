/**
 * UsersService
 * developed by: Luca Chiesa
 */


//Dependencies
const { NotFound } = require('http-errors')
const User = require("./user.entity")


//Service
class UsersService {
  constructor() {
    this.users = []
  }

  findAll() {
    return this.users.map(user => user.toJson())
  }

  getInstance(options) {
    let user = null
    if (!options) return null
    if (options.id) user = this.users.find(user => user.id === +options.id)
    else if (options.email) user = this.users.find(user => user.email === options.email)
    return user
  }

  findById(id) {
    if (!this.users.length) throw NotFound(`No users found`)
    const user = this.users.find(user => user.id === +id)
    console.log(user)
    if (!user) throw NotFound(`User with id #${id} not found`)
    return user.toJson()
  }

  userExists(email) {
    if (!this.users.length) return null
    const user = this.users.find(user => user.email === email)
    if (!user) throw NotFound(`User with email ${email} not found`)
    return user.toJson()
  }

  findByEmail(email) {
    if (!this.users.length) throw NotFound(`No users found`)
    const user = this.users.find(user => user.email === email)
    if (!user) throw NotFound(`User with email ${email} not found`)
    return user.toJson()
  }

  async create(body) {
    const { email, password } = body
    const user = new User(email, password)
    user.password = await user.hashPassword()
    user.token = await user.generateToken()
    this.users = [...this.users, user]
    return user.toJson({ token: true })
  }

  update(id, body) {
    if (!this.users.length) throw NotFound(`No users found`)
    const user = this.users.find(user => user.id === +id)
    const validKeys = ['email', 'password']
    for (let key of validKeys) {
      if (body[key]) user[key] = body[key]
    }
    return user.toJson()
  }

  remove(id) {
    if (!this.users.length) throw NotFound(`No users found`)
    const user = this.users.find(user => user.id === +id)
    if (!user) throw NotFound(`User with id #${id} not found`)
    this.users = this.users.filter(user => user.id !== +id)
  }
}

//Exports
module.exports = new UsersService()
