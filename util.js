const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

const secret = 'MYSUPERSECRET'

const hashPassword = password => {
  return new Promise((resolve, reject) => {
    bcrypt.hash(password, 10, (err, hash) => {
      if (err) reject(err)
      else resolve(hash)
    })
  })
}

const comparePassword = (password, hash) => {
  return new Promise((resolve, reject) => {
    bcrypt.compare(password, hash, (err, result) => {
      if (err) reject(err)
      else resolve(result)
    })
  })
}

const generateToken = payload => jwt.sign(payload, secret)

const verifyToken = token => {
  return new Promise((resolve, reject) => {
    jwt.verify(token, secret, (err, decoded) => {
      if (err) reject(err)
      else resolve(decoded)
    })
  })
}

module.exports = {
  hashPassword,
  comparePassword,
  generateToken,
  verifyToken
}