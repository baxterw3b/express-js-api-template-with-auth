/**
 * AutoLoader
 * developed by: Luca Chiesa
 *
 * Params
 * @param {Express App} app
 */

//Dependencies
const fs = require('fs')

const autoloader = app => {
  try {
    const api = fs.readdirSync('./api')
    for (let folder of api) {
      if (fs.existsSync(`./api/${folder}/index.js`)) {
        const routes = require(`./api/${folder}`)
        console.log(`Routes for /${folder} loaded succesfully`)
        app.use(`/${folder}`, routes)
      }
    }
  } catch (err) {
    console.log(`Error from Loader: ${err.message}`)
  }
}

module.exports = autoloader
