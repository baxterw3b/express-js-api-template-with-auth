//Dependencies
require('express-async-errors')
const express = require('express')
const Middleware = require('./middlewares')

//Create express app
const app = express()

app.use(express.json())

//Autoload all the routes
require('./autoloader')(app)

//NotfOund and Errors middlewares
app.use(Middleware.notFound)
app.use(Middleware.errors)

//App listener
app.listen(3000, () => console.log('App started on port 3000'))